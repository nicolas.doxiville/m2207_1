package tp1;

public class ClientMultiComptes
{
	//Attributs
	private String nom, prenom;
	private Compte[] tabcomptes = new Compte[10];
	private int nbComptes = 0;
	
	//Constructeurs
	public ClientMultiComptes (String n, String p, Compte compte)
	{
		this.tabcomptes[0] = compte;
		this.nbComptes++;
		
		this.nom = n;
		this.prenom = p;
	}
	
	//Accesseurs
	double getSolde()
	{
		double somme = 0;
		for(int i=0; i<nbComptes; i++)
		{
			somme += this.tabcomptes[i].getSolde();
		}
		return somme;
	}
	
	/*Méthodes*/
	
	void ajouterCompte(Compte c)
	{
		this.tabcomptes[this.nbComptes] = c;
		this.nbComptes++;
	}
	
	
	void afficherEtatClient()
	{
		System.out.println("Client : " + this.nom +" "+ this.prenom);
		for(int i=0; i < this.nbComptes; i++)
		{
			System.out.println("Compte : "+this.tabcomptes[i].getNumero() + " - Solde : " + this.tabcomptes[i].getSolde());
		}
		System.out.println("Solde Client : " + this.getSolde());
	}

}
