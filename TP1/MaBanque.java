package tp1;

public class MaBanque
{
	public static void main(String[] args)
	{
		Compte c2 = new Compte(2);
		
		c2.depot(1000);
		c2.afficherSolde();
		
		System.out.println(c2.retrait(600));
		c2.afficherSolde();
		
		System.out.println(c2.retrait(700));
		c2.afficherSolde();
		
		c2.setDecouvert(500);
		
		System.out.println(c2.retrait(700));
		
		
		//Exercice 3.4
		Client client1 = new Client("Musk", "Elon", c2);
		
		//On vérifie que le compte c2 est à découvert
		client1.afficherSolde();
		
		
		//Exercice 4.4
		Compte c10 = new Compte(10);
		c10.depot(1000);
		
		//Création d'un client multicomptes possédant le compte 10
		ClientMultiComptes clientMulti = new ClientMultiComptes("Ellis", "Tom", c10);
		
		Compte c20 = new Compte(20);
		c20.depot(2500);
		clientMulti.ajouterCompte(c20);		
		
		Compte c30 = new Compte(30);
		c30.depot(450);
		clientMulti.ajouterCompte(c30);		
		
		clientMulti.afficherEtatClient();
		
		//Exercice 5
		Compte cp1 = new Compte(101);
		Compte cp2 = new Compte(102);
		
		//On crédite le compte cp1 (le cp2 est automatiquement à 0 lors de la création)
		cp1.depot(1000);
		
		System.out.println("Compte cp1 : Solde : "+ cp1.getSolde());
		System.out.println("Compte cp2 : Solde : "+ cp2.getSolde());
		
		//On effectue un premier virement
		System.out.println(cp1.virer(cp2, 600));
		
		System.out.println("Compte cp1 : "+ cp1.getSolde() + " - Compte cp2 : "+cp2.getSolde());
		
		//On effectue un second virement
		System.out.println(cp1.virer(cp2, 600));
		
		System.out.println("Compte cp1 : "+ cp1.getSolde() + " - Compte cp2 : "+cp2.getSolde());
		
	}
}
