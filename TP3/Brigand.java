public class Brigand extends Humain {
	/*Attributs*/
	
	private String look;
	private int nbDameKid , recomp;
	private boolean prison;
	
	/*Constructeurs*/
	public Brigand(String a) {
		super(a);
		look = "mchant";
		prison = false;
		recomp = 100;
		nbDameKid = 0;
		setBoisson("cognac");
	}
	//Accesseurs
	public String quelEstTonNom() {
		
		return nom + " le " + look;
	}
	public int getRecompense() {
		
		return recomp;
		
	}
	//Mthodes
	public void sePresenter() {
		
		super.sePresenter();
		parler("j'ai l'air " + look + " et j'ai enleve " + nbDameKid + " dames .");
		parler("Ma tte est mise  prix  " + recomp + "$");
	}
	public void enleve(Dame dame){
		
		nbDameKid++;
		recomp += 100;
		parler("Ah ah ! " + dame.quelEstTonNom() + ", tu est ma prisonnire !");
		dame.priseEnOtage();
		
	}
	public void emprisonner(Sherif s) {
		
		prison = true;
		parler("Mince, je suis fait ! " + s.quelEstTonNom() + ", tu m'as eu !");
		
	}
	
}
