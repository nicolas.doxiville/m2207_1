import java.util.Vector; //necessaire d'importer cette fonctionnalite afin d'utiliser les vecteurs

public class Cachot {
	//Attributs
	private Vector<Humain> listePrisonniers; //Creation d'un vecteur auquel on ne renseignera que des variables de type Humain
	String nom;

	//Constructeurs
	public Cachot(String nom){
		this.nom=nom;
		listePrisonniers= new Vector<Humain>(); //Creation de l'instance pour le vecteur
	}
	//Methodes
	void mettreEnCellule(Humain h){
		listePrisonniers.add(h); //Commande add afin d'ajouter notre objet
		System.out.println(h.quelEstTonNom() + " est mis dans la cellule n " + listePrisonniers.indexOf(h)); //Index rensignera l'emplacement de l'objet
	}
	void sortirDeCellulle(Humain h){
		listePrisonniers.remove(listePrisonniers.indexOf(h)); //Commande remove afin de retirer notre objet
		System.out.println(h.quelEstTonNom() + " est sorti de sa cellule");
		System.out.println("Il y a " + listePrisonniers.size() + " prisonniers dans le " + nom); //Size renseignera la taille du vecteur
	}
}
