public class Cowboy extends Humain{ 
	//Attributs
	private int popularite;
	private String caracteristique;
	//Constructeurs
	public Cowboy(String nom){
		super(nom);
		boisson="whiskey";
		popularite=0;
	}
	//Accesseurs
	int getPopularite(){ //acceder a la valeur de la variable popularite
		return popularite;
	}
	//Methodes
	void parler(String texte){ //appel de l'accesseur de la classe mere
		System.out.println(super.quelEstTonNom() + " - " + texte);
	}
	String populaire(){ //En fonction de sa popularite, sa caracteristique change
		if (popularite<10){
			return "vaillant";
		}
		else {
			return "le brave";
		}
	}
	void tire(Brigand brigand){
		System.out.println("Le " + populaire() +" " +super.getNom() + " tire sur " + brigand.getNom()+ ". PAN !"); 
		parler("Prend ça, voyou !");
	}
	void libere(Dame dame){
		popularite=popularite+10;
		dame.estLiberee();
	}

	void sePresenter(){
		super.sePresenter();
		parler("Je suis " + populaire() + " et ma popularite est de " + popularite);
	}	
}
