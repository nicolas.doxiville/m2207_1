public class Dame extends Humain {
	//Attributs
	
	private boolean libre = true;
	
	//Constructeurs
	public Dame(String a) {
		
		super(a);
		libre = true;
		setBoisson("Martini");
		
	}
	//Accesseurs
	public String quelEstTonNom() {
		
		return "Miss " + nom;
	}
	//Mthode
	 public void priseEnOtage() {
		 
		 libre = false;
		 parler("Au secours !");
		 
	 }
	 public void estLiberee() {
		 
		 libre = true;
		 parler("Merci Cowboy");	 
	 }
	 public void sePresenter() {
		 String p1;
					if (libre == true) { p1 = "libre"; }
					else { p1 = "kidnappe"; }
				
			super.sePresenter();
			parler("Actuellement, je suis " + p1);
			
		}
}
