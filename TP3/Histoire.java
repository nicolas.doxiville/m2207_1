public class Histoire {

	public static void main(String[] args) {
		//Declaration
		Humain humain1;

		humain1 = new Humain("klaus");

		//Humain1
		humain1.sePresenter();;
		humain1.boire();

		//Dames
		//Declaration
		Dame dame;

		dame = new Dame("Asuna");

		//Affichage 
		dame.sePresenter();
		dame.priseEnOtage();
		dame.estLiberee();

		//Brigand
		Brigand brigand;


		brigand = new Brigand("dalton");

		//Affichage dalton et modification affichage 3 lignes
		brigand.sePresenter();

		//Cowboy
		Cowboy cowboy;


		cowboy = new Cowboy("Ezio");

		//Affichage Ezio
		cowboy.sePresenter();

		//Enlevement
		brigand.enleve(dame);
		brigand.sePresenter();

		//Cowboy Tire
		cowboy.tire(brigand);

		//Cowboy libere
		cowboy.libere(dame);
		dame.sePresenter();

		//Sherif
		//Declaration
		Sherif sherif;


		sherif = new Sherif("philippe");

		//Affichage de sherif
		sherif.sePresenter();
		sherif.coffrer(brigand);
		brigand.emprisonner(sherif);
		sherif.sePresenter();

		Cowboy clint;

		clint = new Sherif("Clint");


		clint.sePresenter();

		Prison prison;


		prison = new Prison("Alkatraz");


		prison.compterLesPrisonniers();
		prison.mettreEnCellule(brigand);
		prison.compterLesPrisonniers();
		prison.sortirDeCellule(brigand);
		prison.compterLesPrisonniers();
		System.out.println(" ");

		Cachot cachot;

		cachot = new Cachot("Moderne");

		cachot.compterLesPrisonniers();
		cachot.mettreEnCellule(dame);
		cachot.mettreEnCellule(cowboy);
		cachot.compterLesPrisonniers();
		cachot.sortirDeCellule(dame);
		cachot.sortirDeCellule(cowboy);
		cachot.compterLesPrisonniers();
	}

}
