public class Humain {
	
	//Attributs
	
	protected String nom;
	protected String boisson;
	
	//Constructeurs
	
	public Humain(String a) {
		
		nom = a;
		boisson = "lait";
		
	}
	
	//Accesseurs
	
	public String quelEstTonNom() {
		
		return nom;
	}
	
	public String quelEstTaBoisson() {
		
		return boisson;
	}
	
	public void setBoisson(String a) {
		
		boisson = a;
		
	}
	
	//Mthodes
	
	public void parler(String a) {
		
		System.out.println( "(" + nom + ")"+" - " + a);
		
	}
	
	public void sePresenter() {
		
	    parler("Bonjour, je suis " + quelEstTonNom() + " et ma boisson prfre est le " + quelEstTaBoisson() );
		
	}
	
	public void boire() {
		
		parler("Ah ! un bon verre de " + quelEstTaBoisson() + " ! GLOUPS ! " );
		
	}
	
	
	

}
