public class Combat {

	public static void main(String[] args) {

		Pokemon pokemon;
		Pokemon pokemon1;
		String vainqueur = null;


		pokemon = new Pokemon("Pikachu");
		pokemon1 = new Pokemon("bulbizarre");


		System.out.println("Exercice 3");
		System.out.println(" ");
		pokemon.sePresenter();
		pokemon1.sePresenter();
		System.out.println("- - - - - - - - - - - - - - - - - - ");
		int round = 0;
		while(pokemon.isAlive() && pokemon1.isAlive()) {
			pokemon.attaquer(pokemon1);
			pokemon1.attaquer(pokemon);
			round = round +1;
			System.out.println("Round " + round + " " + pokemon.getNom() + " : (en)" + pokemon.getEnergie() + " (atk)" + pokemon.getPuissance() + " - " + pokemon1.getNom() + " : (en)" + pokemon1.getEnergie() + " (atk)" + pokemon1.getPuissance());
		}

		//Affiche vainqueur
		if(pokemon.isAlive()) {
			vainqueur = pokemon.getNom();
		}
		else {
			if(pokemon1.isAlive()) {
				vainqueur = pokemon1.getNom();			
			}
			else {
				vainqueur = "Aucun pokmon ne ";
			}
		}
		System.out.println("- - - - - - - - - - - - - - - - - - ");
		System.out.println(vainqueur + " gagne en " + round + " rounds");
	}
}
