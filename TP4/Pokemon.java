public class Pokemon {
	//Attribut
	private int energie, maxEnergie;
	private String nom;
	private int cycles;
	private int puissance;


	public Pokemon(String n) {
		nom = n;
		maxEnergie = 50 + (int)(Math.random() * ((90 - 50) + 1));
		energie = 30 + (int)(Math.random() * ((maxEnergie - 30) + 1));
		puissance = 10 + (int)(Math.random() * ((30 - 10) + 1));
	}


	public String getNom() {
		return nom;
	}
	public int getEnergie() {
		return energie;
	}
	public int getPuissance() {
		return puissance;
	}


	public void parler(String texte) {
		System.out.println("Je suis " + nom + texte);
	}
	public void sePresenter() {
		parler(", j'ai " + energie + " points d'energie " + "(" + maxEnergie + " max) et une puissance de " + puissance );
	}

	//Manger et vivre
	public void manger() {
		energie = energie +10 + (int)(Math.random() * ((30-10) + 1));
		if (energie > maxEnergie) {
			energie = maxEnergie;
		}
	}
	public void vivre() {
		energie = energie - (20 + (int)(Math.random() * ((40 - 20) + 1)));
		if (energie < 0) {
			energie = 0;
		}
	}


	public boolean isAlive() {
		if (energie > 0) {
			return true;
		}
		else return false;
	}


	public void cycles() {
		cycles = 0;
		while (energie > 0) {
			manger();
			vivre();
			cycles++;
			this.sePresenter();
		}
		parler(", j'ai vcu " + cycles + " cycles");
	}


	public void perdreEnergie(int perte) {
		energie = energie - perte;
		if (energie < 0){
			energie = 0;		
		}

		if(energie < maxEnergie * 0.25) {
			energie = energie - ((perte * 3)/2);
		}
		if(energie < 0) {
			energie = 0;
		}
	}

	//Methode attaquer le pokmon adverse
	public void attaquer(Pokemon adversaire) {
		puissance = puissance - (0 + (int)(Math.random() * ((1-0) +1)));
		adversaire.perdreEnergie(this.getPuissance());
		if(puissance <= 0) {
			puissance = 1;
		}

		if(energie < maxEnergie * 0.25) {
			puissance = puissance * 2;
		}
	}
}
