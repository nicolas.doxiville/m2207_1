//Import des bibliothque
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.*;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.GridLayout;

public class CompteurDeClic extends JFrame implements ActionListener {
	//Attribut
	JButton b;
	JLabel monLabel;
	int a = 0;

	//Constructeur
	//Cration de la fentre
	public CompteurDeClic() {
		super();
		this.setTitle("Mes clik");
		this.setSize(200,100);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fentre est cre!");

		//Cration du bouton
		b = new JButton("BOUTON 0");

		//Ajout de Container
		Container panneau = getContentPane();

		//Ajout de setLayout
		panneau.setLayout(new GridLayout());

		//Ajout de la composante graphique de Container
		panneau.add(b);

		//Ajout de JLabel
		monLabel = new JLabel("Vous avez cliqu 0 fois");
		panneau.add(monLabel);

		// Ajout de la mthode  excuter lors dune action.
		b.addActionListener(this);

		//setVisible
		this.setVisible(true);// Toujours  la fin du constructeur
	}

	public static void main(String[] args) {
		CompteurDeClic app = new CompteurDeClic() ;
	}

	public void actionPerformed(ActionEvent e) {
		System.out.println("Une action a t dtecte");
		a = a+1;
		monLabel.setText("Vous avez cliqu " + a + " fois");

	}
}
