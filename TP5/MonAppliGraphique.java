import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.GridLayout;

public class MonAppliGraphique extends JFrame {
	//Attribut
	JButton b0, b1,b2, b3, b4;
	JLabel monLabel;
	JTextField monTextField;

	//Constructeur
	//Cration de la fentre
	public MonAppliGraphique () {
		super();
		this.setTitle("Ma premire application");
		this.setSize(400,200);
		this.setLocation(20,20);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		System.out.println("La fentre est cre!");

		//Cration du bouton
		b0 = new JButton("BOUTON 0");
		b1 = new JButton("BOUTON 1");
		b2 = new JButton("BOUTON 2");
		b3 = new JButton("BOUTON 3");
		b4 = new JButton("BOUTON 4");

		//Ajout de Container
		Container panneau = getContentPane();

		//Ajout de setLayout
		panneau.setLayout(new GridLayout(3, 2));
		
		//Ajout de la composante graphique de Container
		panneau.add(b0, BorderLayout.NORTH);
		panneau.add(b1, BorderLayout.WEST);
		panneau.add(b2);
		panneau.add(b3, BorderLayout.EAST);
		panneau.add(b4, BorderLayout.SOUTH);

		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		MonAppliGraphique app = new MonAppliGraphique () ;
	}

}
