import java.io.PrintWriter; //Permet d'utiliser les commandes relatives au PrintWriter
import java.net.Socket; //Permet d'utiliser les commandes relatives au socket client

public class MonClient {

	public static void main(String[] args) {
		Socket monSocket; 
		PrintWriter monPrintWriter;
		try{
			monSocket = new Socket("localhost", 8888); 
			System.out.println("Client: " + monSocket); 
			//monSocket.close(); 
			monPrintWriter = new PrintWriter(monSocket.getOutputStream());
			System.out.println("Envoi du message : Hello World");
			monPrintWriter.println("Hello World"); //Envoi du message
			monPrintWriter.flush();
			//flush permet de vider les buffers d'criture vers le medium de sortie
		}
		catch (Exception e){
			System.out.println("Erreur de creation de socket");
			e.printStackTrace();
		}
	}

}
