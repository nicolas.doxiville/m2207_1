import java.io.BufferedReader; //Permet d'utiliser les commandes relatives au Buffer Reader
import java.io.InputStreamReader; //Permet d'utiliser les commandes relatives au StreamReader
import java.net.ServerSocket; //Permet d'utiliser les commandes relatives au socket server
import java.net.Socket;//Permet d'utiliser les commandes relatives au socket client

public class MonServeur {

	public static void main(String[] args) {
		ServerSocket monServerSocket;
		Socket monSocketClient;
		BufferedReader monBufferedReader;  
		try {    
			monServerSocket = new ServerSocket(8888); //Creation d'un objet serveur sur le port 8888 
			System.out.println("ServerSocket: " + monServerSocket);     
			//monServerSocket.close(); 
			monSocketClient = monServerSocket.accept(); //Accepte connexion entrante du client
			System.out.println("Le client s'est connecte");
			//monSocketClient.close();
			monBufferedReader = new BufferedReader(new InputStreamReader(monSocketClient.getInputStream())); //Permet envoi de message
			String message = monBufferedReader.readLine(); //Lire le message
			System.out.println("Message : " + message); 
		} 
		catch (Exception e) {   
			e.printStackTrace();  
		}   
	}
}
