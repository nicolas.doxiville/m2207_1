import java.awt.event.*; //Utiliser des evenements
import java.awt.Container; //Utiliser Container de fenetre et Composants graphiques
import java.awt.BorderLayout; //Utiliser BorderLayout

import java.io.PrintWriter; //Permet d'utiliser les commandes relatives au PrintWriter

import java.net.Socket; //Permet d'utiliser les commandes relatives au socket client

import javax.swing.JFrame; //Utiliser les fonctions de la classe JFrame
import javax.swing.JTextField; //Utiliser les TextField
import javax.swing.JButton; //Utiliser des boutons

public class PanneauClient extends JFrame implements ActionListener{
	//Attributs
	JButton envoyer;
	JTextField champs;
	Container panneau;
	PrintWriter monPrintWriter;
	Socket client;

	//Constructeurs
	public PanneauClient(){
		super();
		this.setTitle("Client - Panneau d'affichage");
		this.setSize(350,120); //Par defaut, fenetre de taille 350x120
		this.setLocation(120,120); //Position (120,120) de l'ecran
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //gestion fermeture de fenetre
		envoyer = new JButton("Envoyer");
		champs = new JTextField();
		panneau = getContentPane();
		panneau.add(champs);
		panneau.add(envoyer, BorderLayout.SOUTH);
		envoyer.addActionListener(this);
		try{
			client = new Socket("localhost", 8888); 
			System.out.println("Client: " + client); 
		}
		catch (Exception e){
			System.out.println("Erreur connexion client");
			e.printStackTrace();
		}
		this.setVisible(true); //Affiche contenu de la fenetre
	}

	public static void main(String[] args) {
		PanneauClient app = new PanneauClient();
	}

	public void actionPerformed(ActionEvent e) {
		System.out.println("Envoi d'un message");
		emettre();
	}

	public void emettre(){
		try{
			monPrintWriter = new PrintWriter(client.getOutputStream());
			monPrintWriter.println(champs.getText()); //Recuperer le texte du champs pour l'envoi
			monPrintWriter.flush();
			//flush permet de vider les buffers d'écriture vers le medium de sortie
			champs.setText("");
		}
		catch (Exception e){
			System.out.println("Erreur d'envoi de message");
			e.printStackTrace();
		}
	}
}
