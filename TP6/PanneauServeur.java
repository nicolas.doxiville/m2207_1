import javax.swing.JFrame; //Utiliser les fonctions de la classe JFrame
import javax.swing.JButton; //Utiliser des boutons
import javax.swing.JScrollPane; //Utiliser le scrolling
import javax.swing.JTextArea; //Utiliser les TextArea

import java.awt.BorderLayout; //Utiliser BorderLayout
import java.awt.event.*; //Utiliser des evenements
import java.awt.Container; //Utiliser Container de fenetre et Composants graphiques
import java.io.BufferedReader; //Permet d'utiliser les commandes relatives au BufferReader
import java.io.InputStreamReader; //Permet d'utiliser les commandes relatives au InputStreamReader
import java.net.ServerSocket; //Permet d'utiliser les commandes relatives au socket server
import java.net.Socket; //Permet d'utiliser les commandes relatives au socket client


public class PanneauServeur extends JFrame implements ActionListener{
	//Attributs
	JButton exit;
	JTextArea texte;
	Container panneau;
	BufferedReader monBufferedReader;
	ServerSocket server;
	Socket client;
	JScrollPane scrolling;

	//Constructeurs
	public PanneauServeur(){
		super();
		this.setTitle("Serveur - Panneau d'affichage");
		this.setSize(400,300); //Par defaut, fenetre de taille 400x300
		this.setLocation(20,20); //Position (20,20) de l'ecran
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //gestion fermeture de fenetre
		exit= new JButton("Exit"); //Instanciation du boutton
		texte= new JTextArea("Le Panneau est actif"); //Instanciation du JTextArea
		scrolling = new JScrollPane(texte); //On met en paramètre le TexteArea
		//scrolling.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		panneau = getContentPane();
		panneau.add(exit, BorderLayout.SOUTH);
		panneau.add(scrolling); //Scrolling contient a la fois le TexteArea et la fonction de scrolling
		exit.addActionListener(this);
		try {
			server = new ServerSocket(8888); //Creation d'un objet serveur sur le port 8888
			System.out.println("ServerSocket: " + server);
			texte.append("\nLe serveur a ete cree");
		}
		catch (Exception e){
			texte.append("\nErreur de creation ServerSocket");
			e.printStackTrace();
		}
		this.setVisible(true); //Affiche contenu de la fenetre
		ecouter();
	}

	public static void main(String[] args) {
		PanneauServeur app = new PanneauServeur();
	}

	public void actionPerformed(ActionEvent e) {
		System.exit(-1); //Instruction fermeture de la fenetre
	}

	public void ecouter(){
		texte.append("\nServeur en attente de connexion");
		try{
			client = server.accept(); //Accepte connexion entrante du client
			texte.append("\nLe client s'est connecte");
			monBufferedReader = new BufferedReader(new InputStreamReader(client.getInputStream())); //Permet envoi de message

			String ligne;
			while ((ligne = monBufferedReader.readLine()) !=null) { //Permet d'afficher plusieurs lignes
				texte.append("\nMessage recu : " + ligne);
			}
		}
		catch (Exception e){
			texte.append("\nErreur dans le traitement de la connexion");
			e.printStackTrace();
		}
	}

}
