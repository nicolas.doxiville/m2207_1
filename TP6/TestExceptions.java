public class TestExceptions {

	public static void main(String[] args) {
		System.out.println("Exercice 1.3");
		int x=2, y=0;
		try{  
			System.out.println("y/x = " + y/x);  
			System.out.println("x/y = " + x/y); //Cette ligne etant non fonctionnelle, elle sera simplement ignoree  
			System.out.println("Commande de fermeture du programme"); 
			/*
			 * Cette ligne ne s'execute pas puisqu'elle se situe apres une instruction non-fonctionnelle
			 * A la moindre erreur, le programme sort du bloc d'instruction try.
			 */
		}
		catch (Exception e){
			System.out.println("Exception detectee");
		}
		finally { //bloc qui s'execute quoi qu'il arrive
			System.out.println("Commande de fermeture du programme");
		}
	}

}
